var products="";;
 var favourites=[];

fetch("http://localhost:3000/products")
.then(response=>response.json())
.then(data=>{console.log(data)
     products=data;
     getProducts();
})

function getProducts(){
    var showproducts="";
    showproducts+="";
    for(var i=0;i<products.length;i++){
        showproducts+=`<div class="list-group-item d-flex flex-column align-item-center">
        <img src=${products[i].thumbnail}><br>
        <h3>${products[i].title}</h3>
        <p style="background-color:green; width:65px; color:white; padding:4px; border:1px solid green; border-radius: 4px;">${products[i].rating}<i class="bi bi-star-fill" style="color:white;fill:white"></i></p>
        <p style="color:red; font-style:italic;">"${products[i].description}"</p>
        <p style="font-size: x-large;"><strong>₹</strong>${products[i].price}</p>
        <p>In-Stock:${products[i].stock}</p>
        <p style="color:green;"><strong>${products[i].category}!!</strong></p>
        <button class ="btn btn-primary" type="button" onclick="addsaveforlater(${products[i].id})">Save For Later <i class="bi bi-heart-fill" style="color:red;"></i></button>
        </div>`
    }
    document.getElementById("products").innerHTML=showproducts;
}
 const getproductInfoById=(x)=>{
    for(var i=0;i<products.length;i++){
        if(x==products[i].id){
            console.log(products[i])
            return products[i];
        }
    }
 }
function addsaveforlater(productId){
   console.log(productId);
   var productObj=getproductInfoById(productId);
   
   var againadded=favourites.find(y=>{
    if(y.id==productId){
      return y;
    }
  })
  if(againadded){
    alert("Already added in favouriteList")
  }
  else{
   console.log("post method");
   fetch("http://localhost:3000/saveforLater",
   {
    method:'POST',
    body:JSON.stringify(productObj),
    headers:{'Content-type':'application/json'}
   }
   )
   .then(response=>response.json())
   .then(data1=>{console.log(data1)})
}
}

fetch("http://localhost:3000/saveforLater")
.then(response=>response.json())
.then(data=>{
    favourites=data;
    getsaveforlater();
})

function getsaveforlater(){
   var showsaveforlater="";
   showsaveforlater+="";
    for(var i=0;i<favourites.length;i++){
        showsaveforlater+=`<div class="list-group-item d-flex flex-column align-item-center">
        <img src=${favourites[i].thumbnail}><br>
        <h3>${favourites[i].title}</h3>
        <p style="background-color:green; width:65px; color:white; padding:4px; border:1px solid green; border-radius: 4px;">${favourites[i].rating}<i class="bi bi-star-fill" style="color:white;fill:white"></i></p>
        <p style="color:red; font-style:italic;">"${favourites[i].description}"</p>
        <p style="font-size: x-large;"><strong>₹</strong>${favourites[i].price}</p>
        <p>In-Stock:${favourites[i].stock}</p>
        <p style="color:green;"><strong>${favourites[i].category}!!</strong></p>
        <div class="carts" style="display:flex; gap:10px;">
        <button class ="btn btn-primary" type="button"><i class="bi bi-cart-fill" style="color:white;fill:white"></i>Add To Cart</button>
        <button class ="btn btn-primary" type="button"><i class="bi bi-emoji-smile-fill"></i>Buy Now</button>
        </div>
        </div>`
    }
    document.getElementById("favouritesList").innerHTML=showsaveforlater;
    }